# 4me API


<a name="overview"></a>
## Overview
Let your transactions work for you!


### Version information
*Version* : v1


### Contact information
*Contact* : The Fantastic 4


### License information
*License* : MIT  
*License URL* : http://opensource.org/licenses/MIT  
*Terms of service* : null


### URI scheme
*Host* : 4me.fun  
*BasePath* : /v1  
*Schemes* : HTTP



