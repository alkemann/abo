
<a name="user-top-3"></a>
### Get user's top 3 categories
```
GET /user/{userId}/top3
```


#### Description
Get a list of top 3 categories (market niches) that the user is interested in based on their transactions.


#### Parameters

|Type|Name|Description|Schema|Default|
|---|---|---|---|---|
|**Header**|**Accept**  <br>*required*|Only acceptable value is `application/vnd.api+json`|string|`"application/vnd.api+json"`|
|**Path**|**userId**  <br>*required*|Id of user in 4me|integer|`2`|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|List of categories|< [Category](../objects/Category.md#category) > array|
|**401**|Unauthorized request|No Content|
|**404**|User not found or no access to it|No Content|


#### Produces

* `application/vnd.json+api`


#### Example HTTP request

##### Request path
```
/user/0/top3
```


##### Request header
```
json :
"string"
```


#### Example HTTP response

##### Response 200
```
json :
[ {
  "id" : "\"BAR\"",
  "type" : "category",
  "attributes" : "object"
} ]
```



