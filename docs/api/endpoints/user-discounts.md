
<a name="user-discounts"></a>
### Get user's available discounts
```
GET /user/{userId}/discounts
```


#### Description
What campaign discounts is the user eligble for right now? Returns a list of campaigns and the merchant data.


#### Parameters

|Type|Name|Description|Schema|Default|
|---|---|---|---|---|
|**Header**|**Accept**  <br>*required*|Only acceptable value is `application/vnd.api+json`|string|`"application/vnd.api+json"`|
|**Path**|**userId**  <br>*required*|Id of user in 4me|integer|`2`|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|List of campaign objects with merchant info|< [Campaign](../objects/Campaign.md#campaign) > array|
|**401**|Unauthorized request|No Content|
|**404**|User not found or no access to it|No Content|


#### Produces

* `application/vnd.json+api`


#### Example HTTP request

##### Request path
```
/user/0/discounts
```


##### Request header
```
json :
"string"
```


#### Example HTTP response

##### Response 200
```
json :
[ {
  "id" : 15,
  "type" : "campaign",
  "attributes" : "object"
} ]
```



