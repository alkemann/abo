
<a name="campaign"></a>
### Campaign

|Name|Description|Schema|
|---|---|---|
|**attributes**  <br>*optional*|NB Beta has extra fields  <br>**Example** : `"object"`|[attributes](#campaign-attributes)|
|**id**  <br>*optional*|Primary key of campaign  <br>**Example** : `15`|integer|
|**type**  <br>*optional*|Type of object, part of JSON-API spec  <br>**Example** : `"campaign"`|string|

<a name="campaign-attributes"></a>
**attributes**

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|QR code hash for verification  <br>**Example** : `"72eb3c17129be46a61b35b4f2982989430cd2cf5"`|string|
|**description**  <br>*optional*|What does the campaign offer?  <br>**Example** : `"90% off next purchase!"`|string|
|**end_date**  <br>*optional*|Last day offer is valid  <br>**Example** : `"2017-12-24"`|string|
|**relationships**  <br>*optional*|**Example** : `"object"`|[relationships](#campaign-relationships)|
|**title**  <br>*optional*|Name of Campaign  <br>**Example** : `"Blackfriday Crazyness"`|string|

<a name="campaign-relationships"></a>
**relationships**

|Name|Description|Schema|
|---|---|---|
|**merchant**  <br>*optional*|**Example** : `"[merchant](#merchant)"`|[Merchant](Merchant.md#merchant)|



