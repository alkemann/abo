
<a name="categoryenum"></a>
### CategoryEnum
Merchants are grouped into niches called Categories

*Type* : enum

- BAR
- BURGER
- GROCERY
- ELECTRO
- RESTAURANT
- CAFE
- STORE
- FASHION



