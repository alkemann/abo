
<a name="category"></a>
### Category

|Name|Description|Schema|
|---|---|---|
|**attributes**  <br>*optional*|**Example** : `"object"`|[attributes](#category-attributes)|
|**id**  <br>*optional*|**Example** : `"[categoryenum](#categoryenum)"`|[CategoryEnum](CategoryEnum.md#categoryenum)|
|**type**  <br>*optional*|Type of object, part of JSON-API spec  <br>**Example** : `"category"`|string|

<a name="category-attributes"></a>
**attributes**

|Name|Description|Schema|
|---|---|---|
|**amount**  <br>*optional*|How much spent on this type of merchant  <br>**Example** : `1900`|integer|
|**count**  <br>*optional*|How many transactions on this type of merchant  <br>**Example** : `5`|integer|
|**title**  <br>*optional*|String representation of category  <br>**Example** : `"Bar"`|string|



