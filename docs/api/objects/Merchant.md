
<a name="merchant"></a>
### Merchant

|Name|Description|Schema|
|---|---|---|
|**attributes**  <br>*optional*|**Example** : `"object"`|[attributes](#merchant-attributes)|
|**id**  <br>*optional*|primary key of merchants  <br>**Example** : `42`|integer|
|**type**  <br>*optional*|Type of object, part of JSON-API spec  <br>**Example** : `"merchant"`|string|

<a name="merchant-attributes"></a>
**attributes**

|Name|Description|Schema|
|---|---|---|
|**address**  <br>*optional*|Place of first store user visited. Campaigned assumed to be active for all franchises  <br>**Example** : `"Jernbanetorget Oslo"`|string|
|**category**  <br>*optional*|**Example** : `"[categoryenum](#categoryenum)"`|[CategoryEnum](CategoryEnum.md#categoryenum)|
|**name**  <br>*optional*|Name of merchant  <br>**Example** : `"Burger King"`|string|



