import React from 'react';
import { observer } from 'mobx-react';

import './FlashMessage.scss';
import { msgStore } from '../../stores';

const FlashMessage = observer(() => {
    const hasMsg = msgStore.message !== null;

    if (hasMsg) {
        setTimeout(() => {
            msgStore.dismiss()
        }, 5000);
    }

    return hasMsg
        ? <div className="FlashMessage" onClick={() => msgStore.dismiss()}>{msgStore.message}</div>
        : null;
});

export default FlashMessage;
