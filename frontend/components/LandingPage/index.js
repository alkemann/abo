import React from 'react';

import './LandingPage.scss';
import Logo from '../../components/Header/logo.svg';

const LandingPage = ({ children }) => {
    return (
        <div className="LandingPage">
            <div className="LandingPage__Main">            
                <div className="LandingPage__Logo">
                    <Logo />
                </div>
                <div className="LandingPage__Body">
                    {children}
                </div>
            </div>
            <div className="LandingPage__Footer">
                © 2017 Fantastic 4&nbsp;&nbsp;
                <a href="/developer">API Docs</a>
            </div>
        </div>
    )
}

export default LandingPage;