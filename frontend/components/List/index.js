import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './List.scss';

const List = ({ items }) => {
    return (
        <div className="List">
            {items.map(item => (
                <div key={item.id} className="List__Item">
                    #{item.id}
                    <Link to={`/campaigns/${item.id}`} style={{flex:1}}>{item.title}</Link>
                </div>
            ))}
        </div>
    )
}

export default List;