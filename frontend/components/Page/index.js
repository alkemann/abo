import React from 'react';

import './Page.scss';

const Page = ({ title, actions, children }) => {
    return (
        <div className="Page">
            <div className="Page__Header">
                <div className="Page__HeaderInner">
                    {title && <h1 className="Page__Title">{title}</h1>}
                    {actions &&
                        <div className="Page__Actions">
                            {actions.map((action, i) =>
                                React.cloneElement(action, { key: i }))}
                        </div>}
                </div>
            </div>
            <div className="Page__Body">
                {children}
            </div>
        </div>
    )
}

export default Page;