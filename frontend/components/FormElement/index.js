import React from 'react';

import './FormElement.scss';

const FormElement = ({ label, children }) => {
    return (
        <div className="FormElement">
            {label && <label className="FormElement__Label">{label}</label>}
            <div className="FormElement__Group">{children}</div>
        </div>
    )
}

export default FormElement;