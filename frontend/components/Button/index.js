import React from 'react';
import { Link } from 'react-router-dom';

import './Button.scss';

export const ButtonLink = (props) => {
    return (
        <Link {...props} className="Button" />
    )
}

const Button = (props) => {
    return (
        <button {...props} className="Button" />
    )
}

export default Button;