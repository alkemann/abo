import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import { mercStore } from '../../stores'; 

import './Header.scss';
import Logo from './logo.svg';

const Header = observer(() => {
    return (
        <div className="Header">
            <div className="Header__Inner">
                <div className="Header__Logo">
                    <Link to="/"><Logo /></Link>
                </div>
                <div className="Header__Menu">
                    <Link to="/campaigns">Campaigns</Link>
                    <Link to="/customers">Customers</Link>
                    <Link to="/trends">Trends</Link>
                    <a href="/developer">API Docs</a>
                </div>
                <div className="Header__User">
                    {mercStore.currentMerchant && mercStore.currentMerchant.name}<br/>
                    <a href="#" className="font-small" onClick={() => mercStore.logOut()}>Log out</a>
                </div>
            </div>
        </div>
    )
})

export default Header;
