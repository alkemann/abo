import React from 'react';

import Header from '../Header';
import FlashMessage from '../FlashMessage';

import './Layout.scss';

const Layout = ({ children }) => {
    return (
        <div className="Layout">
            <header className="Layout__Header">
                <Header />
            </header>
            <main className="Layout__Main">
                <FlashMessage />
                {children}
            </main>
            <footer className="Layout__Footer">
                © 2017 Fantastic 4
            </footer>
        </div>
    )
}

export default Layout;
