import React from 'react';
import { Link } from 'react-router-dom';

import { mercStore } from '../stores';
import api from '../utils/Api';

import Page from '../components/Page';
import List from '../components/List';
import { ButtonLink } from '../components/Button';

export default class Campaigns extends React.Component {
    componentWillMount() {
        api.get(`campaigns?merchant_id=${mercStore.currentId}`)
            .then(campaigns => this.setState({
                loaded: true,
                campaigns
            }));
    }

    state = {
        loaded: false,
        campaigns: []
    }

    render() {
        const { campaigns, loaded } = this.state;

        const actions = [
            <ButtonLink to="/campaigns/new">New campaign</ButtonLink>
        ];

        return (
            <Page
                title="Your campaigns"
                actions={actions}>
                
                {loaded ? (
                    <div className="List">
                        {campaigns.length > 0 ? (
                            campaigns.map(entry => (
                                <div key={entry.id} className="List__Item">
                                    <div style={{ flex: 3 }}>
                                        <div className="font-strong">{entry.title}</div>
                                        <div className="font-small font-truncate">{entry.description}</div>
                                    </div>
                                    <div style={{ flex: 1 }}>
                                        <div className="font-small font-fade">Reach</div>
                                        {entry.matches}
                                    </div>
                                    <div style={{ flex: 1 }}>
                                        <div className="font-small font-fade">End date</div>
                                        {entry.end_date !== null && entry.end_date !== '0000-00-00'
                                            ? entry.end_date
                                            : 'No end date'}
                                    </div>
                                </div>
                            ))
                        ):(
                            <div>No campaigns...</div>
                        )}
                    </div>
                ):(
                    <div>Loading campaigns...</div>
                )}

            </Page>
        )
    }
}