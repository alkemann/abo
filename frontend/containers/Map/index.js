import React from "react";
import { GoogleApiWrapper, Map, Marker, Listing } from 'google-maps-react';
import api from '../../utils/Api';

const currentPos = (opts) => new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject, opts);
});

class MapComponent extends React.Component {
    state = {
        machines: {},
        pos: {}
    }

    fetchMachines = () => {
        api('machines')
            .then(res => {
                this.setState({ machines: res.response.data });
            });
    }

    componentWillMount() {
        this.fetchMachines();

        currentPos().then(({ coords }) => {
            this.setState({
                pos: {
                    lng: coords.longitude,
                    lat: coords.latitude
                }
            })
        });
    }

    fetchCurrentPlace = (mapProps, map) => {
        const { google } = mapProps;
        const service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: this.state.pos,
            radius: '500',
            type: ['restaurant']
        }, console.log);
    }

    render() {
        const { pos, machines } = this.state;
        return Object.keys(pos).length > 0 ? (
            <div>
                <Map
                    onReady={this.fetchCurrentPlace}
                    google={this.props.google}
                    initialCenter={pos}
                    zoom={14}>

                    <Marker
                        name="Dragons"
                        title="You are here"
                        position={pos} />
                    
                    {machines.map((machine, i) => (
                        <Marker
                            key={i}
                            name={machine.attributes.title}
                            title={machine.attributes.title}
                            color="0000ff"
                            icon={{ path: google.maps.SymbolPath.CIRCLE, scale: 5 }}
                            position={{
                                lng: parseFloat(machine.attributes.long, 10),
                                lat: parseFloat(machine.attributes.lat, 10)
                            }} />
                    ))}
                </Map>            
            </div>
        ) : <div>Please wait while we're stalking your ass</div>;
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyC5GhtZe-zGCMvTjj-BA8a9Mn0lTXYXYWM'
})(MapComponent);