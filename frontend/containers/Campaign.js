import React from 'react';
import api from '../utils/Api';

import Page from '../components/Page';

export default class Campaign extends React.Component {
    componentWillMount() {
        api.get(`campaigns/${this.props.match.params.id}`).then(campaign => {
            this.setState({ campaign })
        }).catch(console.error);
    }
    
    state = {
        campaign: {}
    }

    render() {
        const { campaign } = this.state;

        return campaign.id ? (
            <Page title={`Campaign: ${campaign.title}`}>
                <pre>
                    {JSON.stringify(campaign, null, 3)}
                </pre>
            </Page>
        ) : (
            <Page>Loading campaign...</Page>
        )
    }
}