import React from 'react';
import { observer } from 'mobx-react';

import { mercStore } from '../stores';

import LandingPage from '../components/LandingPage';
import FormElement from '../components/FormElement';

@observer
export default class Login extends React.Component {
    handleChange = (e) => {
        const id = e.target.value;
        if (id) {
            mercStore.logIn(id);
        }
    }

    render() {
        return (
            <LandingPage>
                <p>This is a bit crazy</p>
                <FormElement label="Log in with merchant">
                    <select onChange={this.handleChange}>
                        <option>Select merchant</option>
                        {mercStore.merchants.map(m => (
                            <option key={m.id} value={m.id}>{m.name}</option>
                        ))}
                    </select>
                </FormElement>
            </LandingPage>
        )
    }
}