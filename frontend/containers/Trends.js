import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import Page from '../components/Page';
import api from '../utils/Api';

import { mercStore } from '../stores';
const numeral = require('numeral');

const yaxistFormatter = (l) => numeral(l).format('0,0');

export default class Trends extends React.Component {
    componentWillMount() {
        api.get(`trends/${mercStore.currentId}`)
            .then(trends => {
                trends.forEach(trend => {
                    trend.amount = parseInt(trend.amount);
                    trend.month = trend.month;
                    trend.count = parseInt(trend.count);
                });
                return trends;
            })
            .then(trends => {
                this.setState({ trends });
            });
    }
    
    state = {
        trends: []
    }

    render() {
        return (
            <Page title="Trends">
                <h2>Monthly spending</h2>
                <BarChart
                    width={600}
                    height={300}
                    style={{margin: '1rem auto'}}
                    data={this.state.trends}>
                    <XAxis dataKey="month" />
                    <YAxis dataKey="amount" tickFormatter={yaxistFormatter} />
                    <Bar dataKey="amount" fill="#006DED" />
                </BarChart>

                <h2>Monthly customers</h2>
                <BarChart
                    width={600}
                    height={300}
                    style={{ margin: '1rem auto' }}                    
                    data={this.state.trends}>
                    <XAxis dataKey="month" />
                    <YAxis dataKey="count" />
                    <Bar dataKey="count" fill="#006DED" />
                </BarChart>
            </Page>
        )
    }
}