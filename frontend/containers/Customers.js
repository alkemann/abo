import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import { mercStore } from '../stores';
import ReactTable from 'react-table';
import "react-table/react-table.css";

import api from '../utils/Api';
import Page from '../components/Page';
import { ButtonLink } from '../components/Button';

export default class Customers extends React.Component {
    componentWillMount() {
        api.get(`loyal/${mercStore.currentId}`).then(loyal => {
            loyal.forEach(l => {
                l.count = parseInt(l.count);
                l.amount = parseInt(l.amount);
            })
            this.setState({ loyal })
        }).catch(console.error);

        api.get(`potential/${mercStore.currentId}`).then(potential => {
            potential.forEach(p => {
                p.count = parseInt(p.count);
                p.amount = parseInt(p.amount);
            })
            this.setState({ potential })
        }).catch(console.error);
    }

    state = {
        loyal: [],
        potential: []
    }

    render() {
        const { loyal, potential } = this.state;
        return (
            <Page title="Your customers">

                <section style={{ marginBottom: '2rem' }}>
                    <div className="flex">
                        <h2 className="flex1">Loyal customers</h2>
                        <div><ButtonLink to="campaigns/new">New campaign for loyals</ButtonLink></div>
                    </div>

                    <ReactTable
                        defaultPageSize={10}
                        data={loyal}
                        columns={[
                            { Header: 'Gender', accessor: 'gender' },
                            { Header: 'Age', accessor: 'age' },
                            { Header: 'Transactions', accessor: 'count' },
                            { Header: 'Spending in NOK', accessor: 'amount' },
                        ]} />
                </section>

                <section style={{ marginBottom: '2rem' }}>
                    <div className="flex">
                        <h2 className="flex1">Potential customers</h2>
                        <div><ButtonLink to="campaigns/new">New campaign for potentials</ButtonLink></div>
                    </div>

                    <ReactTable
                        defaultPageSize={10}                    
                        data={potential}
                        columns={[
                            { Header: 'Gender', accessor: 'gender' },
                            { Header: 'Age', accessor: 'age' },
                            { Header: 'Transactions', accessor: 'count' },
                            { Header: 'Spending in NOK', accessor: 'amount' },
                        ]} />
                </section>

            </Page>
        )
    }
}