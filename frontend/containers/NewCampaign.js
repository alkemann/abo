import React from 'react';
import { Link } from 'react-router-dom';

import api from '../utils/Api';
import { mercStore, msgStore } from '../stores';

import Page from '../components/Page';
import Button from '../components/Button';
import FormElement from '../components/FormElement';

export default class NewCampaign extends React.Component {
    state = {
        error: '',
        disableNiche: false,
        disableVisit: false,
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const inputs = e.target.querySelectorAll('input, select, textarea');
        const formData = {
            type: "campaigns",
            attributes: {
                merchant_id: mercStore.currentId
            }
        };
        Array.from(inputs).forEach(input => {
            formData["attributes"][input.name] = input.value;
        });
        api.post('campaigns', formData)
            .then(res => {
                msgStore.message = 'Campaign created'
                return res;
            })
            .then(() => {
                setTimeout(() => {
                    this.props.history.push('/campaigns');
                }, 1000);
            });
    }

    disableNiche = (e) => {
        this.setState({ disableNiche: !!e.target.value })
    }

    disableVisit = (e) => {
        this.setState({ disableVisit: !!e.target.value })
    }

    render() {
        const { error } = this.state;

        return (
            <Page title="New campaign">
                <form onSubmit={this.handleSubmit} autoComplete="false">
                    <FormElement label="Campaign title">
                        <input name="title" type="text" required />
                    </FormElement>

                    <FormElement label="Description">
                        <input name="description" type="text" required />
                    </FormElement>

                    <FormElement label="Age range">
                        <input name="min_age" type="number" min="18" placeholder="From" />
                        <input name="max_age" type="number" min="18" placeholder="To" />
                    </FormElement>

                    <FormElement label="Gender">
                        <select name="gender">
                            <option value="BOTH">Both</option>
                            <option value="FEMALE">Female</option>
                            <option value="MALE">Male</option>
                        </select>
                    </FormElement>

                    <FormElement label="Days since last visit">
                        <input name="days_since_visit" type="number" placeholder="eg. 120" onChange={this.disableNiche} disabled={this.state.disableVisit} />
                    </FormElement>

                    <FormElement label="Days since last visit in niche">
                        <input name="days_since_niche" type="number" placeholder="eg. 120" onChange={this.disableVisit} disabled={this.state.disableNiche} />
                    </FormElement>

                    <FormElement label="End date">
                        <input name="end_date" type="date" placeholder="YYYY-MM-DD" />
                    </FormElement>

                    <label className="flex" style={{width:'50%'}}>
                        <input style={{marginRight:'1rem'}} type="checkbox" defaultChecked />
                        <span className="flex1">Show also to other potential users that do not share these characteristics</span>
                    </label>

                    {error &&
                        <div>{error}</div>}

                    <FormElement>
                        <Button>Save campaign</Button>
                    </FormElement>
                </form>
            </Page>
        )
    }
}