import React from 'react';
import api from '../utils/Api';

import Page from '../components/Page';
import { ButtonLink } from '../components/Button';

export default class Customer extends React.Component {
    state = {
        customer: {
            id: 1,
            name: 'Alf Trondsen',
            lastVisit: '2017-09-16',
            customerSince: '2015-09-16',
            averageSpend: 100
        }
    }

    render() {
        const { customer } = this.state;

        return (
            <Page title={customer.name}>
                
                Name: {customer.name}<br/>
                Customer since: {customer.customerSince}<br/>
                Last visit: {customer.lastVisit}<br/>
                Average spend: {customer.averageSpend} NOK<br/>

            </Page>
        )
    }
}