
const handleErrors = (res) => {
    if (res.status >= 400) {
        const message = res.headers.get('x-error-message') || res.statusText;
        const error = new Error(message);
        error.statusCode = res.status;
        error.statusText = res.statusText

        // TODO: Handle this in each component
        console.log(`${error.message}. \n\nError code: ${error.statusCode}\nError message: ${error.statusText}`);

        throw error;
    }

    return res;
}

const consolidateEntries = (res) => {
    if (res.data.attributes) {
        return {
            id: res.data.id,
            ...res.data.attributes
        }
    }

    return Object.keys(res.data).map(key => ({
        id: res.data[key].id,
        ...res.data[key].attributes
    }));
}

const parseResponse = (res) => {
    return res.json();
}

const api = (url, opts) => {
    return fetch(url, opts)
        .then(handleErrors)
        .then(parseResponse)
        .then(consolidateEntries)
        .catch(console.error);
}

const apiFriendlyBody = (type, attributes) => {
    return {
        data: {
            type,
            attributes
        }
    }
}

export default {
    get(url, opts) {
        url = 'v1/' + url;
        console.log('[GET]', url);
        return api(url, Object.assign({
            method: 'GET',
            headers: {
                Accept: 'application/vnd.api+json'
            },
        }, opts));
    },

    post(url, data, opts) {
        url = 'v1/' + url;
        console.log('[POST]', url, data);
        return api(url, Object.assign({
            method: 'POST',
            headers: {
                'Accept': 'application/vnd.api+json',
                'Content-Type': 'application/vnd.api+json'
            },
            body: JSON.stringify(data)
        }, opts))
    }
}
