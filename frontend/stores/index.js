import { action, observable, computed } from 'mobx';

import api from '../utils/Api';

class Merchants {
    @observable currentId = null;
    @observable merchants = [];

    constructor () {
        const session = sessionStorage.getItem('merchant_id');
        
        if (session) {
            this.currentId = session;
        }

        api.get('merchants').then(mercs => {
            this.merchants = mercs;
        });
    }

    @computed get currentMerchant () {
        return this.merchants.filter(m => m.id == this.currentId)[0];
    }

    @action logIn (id) {
        console.log('loggin in');
        this.currentId = id;
        sessionStorage.setItem('merchant_id', id);
    }
    
    @action logOut () {
        console.log('loggin out');
        this.currentId = null;
        sessionStorage.removeItem('merchant_id');
    }
}

class Messages {
    @observable message = null;

    @action dismiss () {
        this.message = null;
    }
}

export const msgStore = new Messages()
export const mercStore = new Merchants();