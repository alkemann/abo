import React from "react";
import {render} from "react-dom";
import {HashRouter,Switch,Route,Redirect} from 'react-router-dom';
import {Provider, observer} from 'mobx-react';

import Campaigns from './containers/Campaigns';
import Campaign from './containers/Campaign';
import NewCampaign from './containers/NewCampaign';
import Customers from './containers/Customers';
import Customer from './containers/Customer';
import Trends from './containers/Trends';
import Login from './containers/Login';
import Layout from './components/Layout';

import * as stores from './stores';

import './styles/base.scss';

const LoginWall = observer(({children}) => {
    return stores.mercStore.currentId !== null ? children : <Login/>
})

render(
    <Provider {...stores}>
        <LoginWall>
            <HashRouter>
                <Layout>
                    <Switch>
                        <Redirect from="/" to="/campaigns" exact />

                        <Route path="/campaigns" exact component={Campaigns} />
                        <Route path="/campaigns/new" component={NewCampaign} />
                        <Route path="/campaigns/:id" component={Campaign} />

                        <Route path="/customers" exact component={Customers} />
                        <Route path="/customer/:id" exact component={Customer} />

                        <Route path="/trends" exact component={Trends} />
                    </Switch>
                </Layout>
            </HashRouter>
        </LoginWall>
    </Provider>,
    document.getElementById('app')
);
