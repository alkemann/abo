<?php

namespace backend;

use alkemann\h2l\Environment;
use alkemann\h2l\Remote;
use backend\entities\Machines;

class Dnb
{
    const ATM_BY_ZIP = '/banks/1.0/bank/atm/zip/';
    const CUSTOMER_BY_ID = '/customers/1.0/customer/';
    const ACCOUNTS_BY_ID = '/accounts/1.0/account/customer/';
    const ACCOUNT_HISTORY = '/accounts/1.0/account';

    protected $base = 'https://dnbapistore.com/hackathon';

    protected function getAuthHeader()
    {
        $token = Environment::get('token');
        return [
            'Authorization' => "Bearer $token",
            'Accept' => 'application/json'
        ];
    }

    public function getRemoteHandler(): Remote
    {
        return new Remote();
    }

    public function getMachines($zip = '0191'): array
    {
        $remote = $this->getRemoteHandler();
        $url = $this->base . self::ATM_BY_ZIP . $zip;
        $headers = $this->getAuthHeader();
        $response = $remote->get($url, $headers);
        if ($response->code() >= 400) {
            throw new \Exception($response->body(), $response->code());
        }
        $data = $response->content();
        return $data['atms'];
    }

    public function getCustomerById(string $ssn): array
    {
        $remote = $this->getRemoteHandler();
        $url = $this->base . self::CUSTOMER_BY_ID . $ssn;
        $headers = $this->getAuthHeader();
        $response = $remote->get($url, $headers);
        if ($response->code() >= 400) {
            throw new \Exception($response->body(), $response->code());
        }
        return $response->content();
    }

    public function getAccountsByCustomerId($id): array
    {
        $remote = $this->getRemoteHandler();
        $url = $this->base . self::ACCOUNTS_BY_ID . $id;
        $headers = $this->getAuthHeader();
        $response = $remote->get($url, $headers);
        if ($response->code() >= 400) {
            return [];
        }
        $data = $response->content();
        return $data['accounts'];

    }

    public function getTransactionHistory($accountId, $customerId): array
    {
        $df = '01012017';
        $dt = '01092017';
        $remote = $this->getRemoteHandler();
        $url = $this->base . self::ACCOUNT_HISTORY . '?'
            . "accountNumber={$accountId}&customerID={$customerId}&dateFrom={$df}&dateTo={$dt}";
        $headers = $this->getAuthHeader();
        $response = $remote->get($url, $headers);
        if ($response->code() >= 400) {
//            throw new \Exception($response->body(), $response->code());
            echo $response->body();
            exit;
        }
        $data = $response->content();
        $transactions = array_filter($data['transactions'], function($v) {
            return $v['transactionType'] == 5;
        });
        return $transactions;
    }
}