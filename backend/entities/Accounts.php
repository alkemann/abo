<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Accounts extends Model
{
    const TYPE_SAVINGS = 'Savings';
    const TYPE_CURRENT = 'Current';

    static $pk = 'accountNumber';
    static $fields = ['accountNumber', 'accountType', 'availableBalance'];

    public static function findByCustomerIdAndType($ssn, $type = self::TYPE_CURRENT)
    {
        $data = array_filter((new Dnb)->getAccountsByCustomerId($ssn), function($v) use ($type) {
            return $v['accountType'] == $type;
        });
        return array_map(function($v) { return new Accounts($v); }, $data);
    }

}