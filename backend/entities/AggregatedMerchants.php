<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

/**
 * Class AggregatedMerchants
 * @package backend\entities
 * @property int $count
 * @property int $amount
 */
class AggregatedMerchants extends Model
{
    static $pk = 'id';
    static $table = 'aggr_users_merchants';
    static $fields = ['id','amount','count','month','user_id','merchant_id'];

    public static function findLoyalCustomersById($id)
    {
        $t = static::$table;
        $mt = Merchants::$table;
        $ut = Users::$table;
        $q = <<<LOYAL
SELECT
  CAST(SUM(am.amount) AS SIGNED) AS amount,
  CAST(SUM(am.count)  AS SIGNED) AS `count`,
  u.gender AS gender,
  u.age AS age
FROM $t AS am
LEFT JOIN $mt AS m ON (am.merchant_id = m.id)
LEFT JOIN $ut AS u ON (am.user_id = u.id)
WHERE am.merchant_id = $id
GROUP BY am.user_id
HAVING `count` > 1
LOYAL;
        $db = static::db();
        return $db->query($q);
    }

    public static function findPotentialCustomersById($id)
    {
        $merchant = Merchants::get($id);
        $nid = $merchant->niche_id;

        $t = static::$table;
        $mt = Merchants::$table;
        $ut = Users::$table;
        $q = <<<LOYAL
SELECT
  CAST(SUM(am.amount) AS SIGNED) AS amount,
  CAST(SUM(am.count)  AS SIGNED) AS `count`,
  u.gender AS gender,
  u.age AS age,
  m.niche_id
FROM $t AS am
LEFT JOIN $mt AS m ON (am.merchant_id = m.id)
LEFT JOIN $ut AS u ON (am.user_id = u.id)
WHERE m.niche_id = '$nid' AND m.id != $id
GROUP BY am.user_id
LOYAL;
        $db = static::db();
        return $db->query($q);
    }

    public static function getMerchantTrend(int $mid): array
    {

        $q = <<<SQL
SELECT SUM(`count`) AS `count`, SUM(amount) AS `amount` , DATE_FORMAT(CONCAT(`month`, '01'), '%b.%Y') AS `month`
FROM `aggr_users_merchants`
WHERE merchant_id = $mid
GROUP BY `month`;
SQL;
        $db = static::db();
        return $db->query($q);

    }
}
