<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Users extends Model
{
    const FEMALE = 'FEMALE';
    const MALE = 'MALE';
    static $pk = 'id';
    static $table = 'users';
    static $fields = ['id', 'gender', 'age', 'reference_id'];

}
