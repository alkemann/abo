<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Merchants extends Model
{
    static $pk = 'id';
    static $table = 'merchants';
    static $fields = ['id', 'niche_id', 'name', 'address'];

    public function jsonSerialize(): array
    {
        $pk = static::$pk;
        $data = $this->data();
        unset($data[$pk]);
        $type = 'merchant';
        $out = [
            'type' => $type,
            'id' => $this->{$pk},
        ];
        foreach ($data as $field => $value) {
            $data[$field] = utf8_encode($value);
        }
        $data['category'] = $data['niche_id'];
        unset($data['niche_id']);
        $out['attributes'] = $data;
        return $out;
    }
}
