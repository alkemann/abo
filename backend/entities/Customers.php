<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Customers extends Model
{
    static $pk = 'customerID';

    public static function get($id, array $conditions = [], array $options = []): Customers
    {
        return new Customers((new Dnb)->getCustomerById($id));
    }

}