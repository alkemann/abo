<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Campaigns extends Model
{
    const GENDER_MALE = 'MALE';
    const GENDER_FEMALE = 'FEMALE';

    static $pk = 'id';
    static $table = 'campaigns';
    static $fields = [
        'id',
        'merchant_id',
        'min_age',
        'max_age',
        'gender',
        'days_since_visit',
        'days_since_niche',
        'title',
        'description',
        'code',
        'status',
        'end_date',
        'matches'
    ];

    public function jsonSerialize(): array
    {
        $pk = static::$pk;
        $data = $this->data();
        unset($data[$pk]);
        $type = 'campaign';
        $out = [
            'type' => $type,
            'id' => $this->{$pk},
            'attributes' => $data
        ];

        if ($this->relationships) {
            $out['relationships'] = $this->relationships;
        }

        return $out;
    }

    public static function findForUser(int $user_id): array
    {
        $t = Transactions::$table;
        $mt = Merchants::$table;
        $ut = Users::$table;

        $matches = [];
        $campaigns = Campaigns::find();
        foreach ($campaigns as $campaign) {

            $merchant = Merchants::get($campaign->merchant_id);


            $wheres = ['t.user_id = ' . (int)$user_id];
            if (is_numeric($campaign->min_age)) {
                $wheres[] = 'u.age >= ' . (int)$campaign->min_age;
            }
            if (is_numeric($campaign->max_age)) {
                $wheres[] = 'u.age <= ' . (int)$campaign->max_age;
            }
            if ($campaign->gender && in_array($campaign->gender, [self::GENDER_FEMALE, self::GENDER_MALE])) {
                $wheres[] = "u.gender = '{$campaign->gender}'";
            }
            if (is_numeric($campaign->days_since_visit)) {
                $wheres[] = "t.merchant_id = '{$campaign->merchant_id}'";
                $i = (int)$campaign->days_since_visit;
                $wheres[] = "t.`date` >  (DATE_SUB(CURDATE(), INTERVAL {$i} DAY))";
            } elseif (is_numeric($campaign->days_since_niche)) {
                $nid = $merchant->niche_id;
                $wheres[] = "m.niche_id = '{$nid}'";
                $i = (int)$campaign->days_since_niche;
                $wheres[] = "t.`date` > (DATE_SUB(CURDATE(), INTERVAL {$i} DAY))";
            }

            $where = join(' AND ', $wheres);
            $q = <<<LOYAL
    SELECT COUNT(1) AS `count`
    FROM $t AS t
    LEFT JOIN $mt AS m ON (t.merchant_id = m.id)
    LEFT JOIN $ut AS u ON (t.user_id = u.id)
    WHERE $where
LOYAL;
            $result = static::db()->query($q);
            $count = (int) $result[0]['count'];
            if ($count > 0) {
                $campaign->relationships = ['merchant' => ['data' => $merchant]];
                $matches[] = $campaign;
            }
        }
        return $matches;
    }

    public function populateMatches(): void
    {
        $t = Transactions::$table;
        $mt = Merchants::$table;
        $ut = Users::$table;

        $wheres = [];
        if (is_numeric($this->min_age)) {
            $wheres[] = 'u.age >= ' . (int) $this->min_age;
        }
        if (is_numeric($this->max_age)) {
            $wheres[] = 'u.age <= ' . (int) $this->max_age;
        }
        if ($this->gender && in_array($this->gender, [self::GENDER_FEMALE, self::GENDER_MALE])) {
            $wheres[] = "u.gender = '{$this->gender}'";
        }
        if (is_numeric($this->days_since_visit)) {
            $wheres[] = "t.merchant_id = '{$this->merchant_id}'";
            $i = (int) $this->days_since_visit;
            $wheres[] = "t.`date` >  (DATE_SUB(CURDATE(), INTERVAL {$i} DAY))";
        } elseif (is_numeric($this->days_since_niche)) {
            $merchant = Merchants::get($this->merchant_id);
            $nid = $merchant->niche_id;
            $wheres[] = "m.niche_id = '{$nid}'";
            $i = (int) $this->days_since_niche;
            $wheres[] = "t.`date` > (DATE_SUB(CURDATE(), INTERVAL {$i} DAY))";
        }

        if (empty($wheres)) {
            return;
        }

        $where = join(' AND ', $wheres);
        $q = <<<LOYAL
SELECT
  COUNT(1) AS `count`
FROM $t AS t
LEFT JOIN $mt AS m ON (t.merchant_id = m.id)
LEFT JOIN $ut AS u ON (t.user_id = u.id)
WHERE $where
LOYAL;
        $result = static::db()->query($q);
        $this->matches = (int) $result[0]['count'];
    }
}
