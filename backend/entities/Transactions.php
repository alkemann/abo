<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Transactions extends Model
{
    static $pk = 'id';
    static $table = 'transactions';
    static $fields = ['id','user_id','merchant_id','amount','currency','date'];

    public static function getDnbTransactions($account, $ssn): array
    {
        return (new Dnb)->getTransactionHistory($account, $ssn);
    }

    public static function getMerchantByTransactionMessage(string $message): Merchants
    {
        $regex = "|Varekjøp ([^\s]+) (.+)Dato|";
        $match = preg_match($regex, $message, $matches);
        if (!$match) {
            die ($message . ' is not a good thing');
        }
        [$k, $name, $address] = $matches;
        $merchant = Merchants::findAsArray(['name' => $name]);
        if (!$merchant) {
            $merchant = new Merchants(compact('name', 'address'));
            $merchant->save();
            return $merchant;
        }
        return current($merchant);
    }

    public static function findByUserWithMerchants(int $user_id): array
    {
        $db = static::db();
        $q = "SELECT * FROM transactions AS t LEFT JOIN merchants AS m ON (t.merchant_id = m.id) WHERE t.user_id = $user_id";
        $result = $db->query($q);
        return $result;
    }
}
