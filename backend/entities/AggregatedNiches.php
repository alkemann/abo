<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class AggregatedNiches extends Model
{
    static $pk = 'id';
    static $table = 'aggr_users_niches';
    static $fields = ['id','user_id','niche_id','amount','count','month'];

    public static function getTopNiches(int $user_id): array
    {
        $t = static::$table;
        $q = <<<TOP
SELECT
    niche_id,
    COUNT(niche_id) AS `count`,
    SUM(amount) AS `amount`
FROM $t AS t
WHERE user_id = $user_id
GROUP BY niche_id
ORDER BY `count` DESC, `amount` DESC
LIMIT 3;
TOP;
        $result = static::db()->query($q);
        $out = array_map(function($v) {
            return [
                'id' => $v['niche_id'],
                'type' => 'category',
                'attributes' => [
                    'title' => ucfirst(strtolower($v['niche_id'])),
                    'amount' => (int) $v['amount'],
                    'count' => $v['count']
                ]
            ];
        }, $result);
        return $out;
    }

}
