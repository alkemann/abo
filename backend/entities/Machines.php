<?php

namespace backend\entities;

use alkemann\jsonapi\Model;
use backend\Dnb;

class Machines extends Model
{
    static $pk = 'atmID';
    public static function getListOfNameAndLocations(): array
    {
        return array_map(function($v) { return new Machines($v); }, (new Dnb)->getMachines());
    }
}