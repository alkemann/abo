<?php

namespace backend;

use alkemann\h2l\{
    Log, Request, Response, Router, util\Http
};
use alkemann\h2l\exceptions\InvalidUrl;
use alkemann\jsonapi\{ Controller, response\Error, response\Result };
use backend\entities\{ AggregatedMerchants, AggregatedNiches, Campaigns, Customers, Machines, Merchants };

/**
 * Class Api
 *
 * @package backend
 */
class Api extends Controller
{
    static $routes = [
        // Url                             function            request method
        ['v1/machines',                    'machines',         Http::GET],
        ['v1/merchants',                   'merchants',        Http::GET],
        ['v1/campaigns',                   'campaigns',        Http::GET],
        ['v1/campaigns',                   'createCampaign',   Http::POST],
        ['%v1/customers/(?<id>\d+)%',      'getCustomer',      Http::GET],
        ['%v1/campaigns/(?<id>\d+)%',      'getCampaign',      Http::GET],

        ['%v1/trends/(?<id>\d+)%',         'merchantTrends',       Http::GET],
        ['%v1/loyal/(?<id>\d+)%',          'loyalCustomers',       Http::GET],
        ['%v1/potential/(?<id>\d+)%',      'potentialCustomers',   Http::GET],

        ['%v1/user/(?<id>\d+)/top3%',      'userTop3',             Http::GET],
        ['%v1/user/(?<id>\d+)/discounts%', 'activeCampaignsForUser', Http::GET],
    ];

    public static function isUrlAPIRequest(string $url): bool
    {
        return (substr($url,0, 3) === 'v1/');
    }

    /////////////////////////

    public function userTop3(Request $request): Response
    {
        $top_niches = AggregatedNiches::getTopNiches($request->param('id'));
        return new Result($top_niches);
    }

    public function activeCampaignsForUser(Request $request): Response
    {
        $active = Campaigns::findForUser($request->param('id'));
        return new Result($active);
    }

    public function merchantTrends(Request $request): Response
    {
        $trends = AggregatedMerchants::getMerchantTrend($request->param('id'));
        $data = array_reduce($trends, function($o, $v) {
            $o[] = [
                'id' => null,
                'type' => 'trend',
                'attributes' => $v
            ];
            return $o;
        }, []);
        return new Result($data);
    }

    public function getCampaign(Request $request): Response
    {
        $campaign = Campaigns::get($request->param('id'));
        if (!$campaign) {
            return new Error([], Http::CODE_NOT_FOUND);
        }
        return new Result($campaign);
    }

    public function loyalCustomers(Request $request): Response
    {
        $id = $request->param('id');
        $loyalCustomers = AggregatedMerchants::findLoyalCustomersById($id);
        $data = array_reduce($loyalCustomers, function($o, $v){
            $o[] = [
                'id' => null,
                'type' => 'loyal',
                'attributes' => $v
            ];
            return $o;
        }, []);
        return new Result($data);
    }

    public function potentialCustomers(Request $request): Response
    {
        $id = $request->param('id');
        $potentialCustomersBy = AggregatedMerchants::findPotentialCustomersById($id);
        $data = array_reduce($potentialCustomersBy, function($o, $v){
            $o[] = [
                'id' => null,
                'type' => 'potential',
                'attributes' => $v
            ];
            return $o;
        }, []);
        return new Result($data);
    }

    public function createCampaign(Request $request): Response
    {
        $body = file_get_contents('php://input');
        $post = json_decode($body, true);
        $data = array_map(function($v) {
            if (!empty($v)) {
                return $v;
            }
            return null;
        }, $post['attributes']);
        $campaign = new Campaigns($data);
        $campaign->code = sha1($campaign->title . uniqid());
        $campaign->populateMatches();
        $campaign->save();
        return new Result($campaign, Http::CODE_CREATED);
    }

    public function campaigns(Request $request): Response
    {

        return new Result(Campaigns::findAsArray($request->getGetData()));
    }

    public function merchants(Request $request): Response
    {
        return new Result(Merchants::findAsArray());
    }

    public function getCustomer(Request $request): Response
    {
        return new Result(Customers::get($request->param('id')));
    }

    public function machines(Request $request): Response
    {
        $data = Machines::getListOfNameAndLocations();
        return new Result($data);
    }

    ///////////////////////////////////////////////////
    // Bootstrapping of the APIs add new routes above
    ///////////////////////////////////////////////////

    protected $config;

    public function __construct(array $config = [])
    {
        $this->config = [
            'header_func' => 'header',
            'router' => Router::class
        ] + $config;
    }

    public function addRoutes(): void
    {
        $router = $this->config['router']; // Should be \alkemann\h2l\Router or compatible
        $router::$DELIMITER = '%';
        foreach (static::$routes as [$url, $func, $method]) {
            $router::add($url, \Closure::fromCallable([$this, $func]), $method);
        }
    }
}
