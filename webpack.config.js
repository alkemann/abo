const webpack = require('webpack');
const path = require('path');

const SRC = path.resolve(__dirname, 'frontend');
const DEST = path.resolve(__dirname, 'webroot/js');
const NODE_MODULES = path.resolve(__dirname, 'node_modules');

module.exports = {
    devtool: 'source-map',
    context: SRC,
    entry: {
        index: ['./index.js']
    },
    output: {
        filename: 'bundle.js',
        path: DEST
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [SRC, NODE_MODULES]
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: SRC,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ["react", "es2015", "stage-2"],
                        plugins: ['transform-decorators-legacy']
                    }
                }],
                
            },
            {
                test: /\.css$/,
                include: [SRC, NODE_MODULES], // for ReactTable
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 0,
                            localIdentName: '[name]__[local]___[hash:base64:5]'
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                include: SRC,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 1,
                            localIdentName: '[name]__[local]___[hash:base64:5]'
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.svg$/,
                use: [{
                    loader: 'react-svg-loader',
                    options: {
                        es5: true,
                        svgo: {
                            plugins: [{
                                removeAttrs: { attrs: 'xmlns.*' }
                            }]
                        }
                    }
                }]
            }
        ]
    }
}



// var webpack = require('webpack');
// var path = require("path");

// var DIST_DIR = path.resolve(__dirname, "webroot/js");
// var SRC_DIR = path.resolve(__dirname, "frontend");

// var config = {
//     entry: SRC_DIR + "/index.js",
//     output: {
//         path: DIST_DIR,
//         filename: "bundle.js",
//         publicPath: "/js/"
//     },
//     debug: true,
//     module: {
//         loaders: [
//             {
//                 test: /\.js?/,
//                 include: SRC_DIR,
//                 loader: "babel-loader",
//                 query: {
//                     presets: ["react", "es2015", "stage-2"]
//                 }
//             }
//         ]
//     },
//     plugins: [
//       new webpack.ProvidePlugin({
//         'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
//       })
//     ]
// };

// module.exports = config;
