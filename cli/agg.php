<?php

use backend\Dnb;
use backend\entities\AggregatedMerchants;
use backend\entities\AggregatedNiches;
use backend\entities\Merchants;
use backend\entities\Transactions;
use backend\entities\Users;

$root = dirname(realpath(__FILE__)) . '/../';

require $root . 'vendor/autoload.php';
require $root . 'configs/environments.php';

$limit = 100;

$users = Users::find([], ['limit' => $limit]);

foreach ($users as $user) {
    /**
     * @var $user Users
     */
//    print_r($user->jsonSerialize());
    $transactions = Transactions::findByUserWithMerchants($user->id);

    $niches = array_reduce($transactions, function($o, $v) {
        $o[$v['niche_id']] = [];
        return $o;
    }, []);

    $merchants = array_reduce($transactions, function($o, $v) {
        $o[$v['merchant_id']] = [];
        return $o;
    }, []);

    foreach ($transactions as $transaction) {
        $m = date('Ym', strtotime($transaction['date']));
        $nid = $transaction['niche_id'];
        $mid = $transaction['merchant_id'];
        if (!isset($niches[$nid][$m])) {
            $niches[$nid][$m] = new AggregatedNiches([
                'month' => $m,
                'amount' => 0,
                'count' => 0,
                'niche_id' => $nid,
                'user_id' => $user->id
            ]);
        }
        if (!isset($merchants[$mid][$m])) {
            $merchants[$mid][$m] = new AggregatedMerchants([
                'month' => $m,
                'amount' => 0,
                'count' => 0,
                'merchant_id' => $mid,
                'user_id' => $user->id
            ]);
        }

        $merchants[$mid][$m]->amount += $transaction['amount'];
        $merchants[$mid][$m]->count += 1;
        $niches[$nid][$m]->amount += $transaction['amount'];
        $niches[$nid][$m]->count += 1;
    }

//    print_r($niches);

    foreach ($niches as $months) {
        foreach ($months as $n) {
            $n->save();
        }
    }
    foreach ($merchants as $months) {
        foreach ($months as $n) {
            $n->save();
        }
    }

    echo ".";
}

