<?php

use backend\Dnb;
use backend\entities\Merchants;
use backend\entities\Transactions;
use backend\entities\Users;

$root = dirname(realpath(__FILE__)) . '/../';

require $root . 'vendor/autoload.php';
require $root . 'configs/environments.php';

$limit = 10;
$dnb = new \backend\Dnb();


//$getDataFromApi = function($ssn) use ($dnb) {
//    $result = $dnb->getCustomerById($ssn);
//    $data = [
//        'gender' => $result['gender'],
//        'reference_id' => $result['customerID']
//    ];
//    $data['age'] = date_diff(date_create($result['dateOfBirth']), date_create('now'))->y;
//    return $data;
//};


//$file = fopen($root . 'data/test_users.csv', 'r');
//$line = fgetcsv($file);

//while ($limit--) {
//    $s = fgets($file);
//while ($s = fgets($file)) {
//    $line = str_getcsv($s, ';', '"');
//
//    // disabled API version for efficiency
////    [$i, $ssn] = explode(';', $line[0]);
////    $data = $getDataFromApi($ssn);
//
//    // data dump version
//    [$ssn, $dob, $gender, $age] = $line;
////    print_r([$ssn, $line]); exit;
//    $data = [
//        'gender' => $gender == 'Male' ? Users::MALE : Users::FEMALE,
//        'reference_id' => $ssn,
//        'age' => $age //date_diff(date_create($line[5]), date_create('now'))->y,
//    ];
//
//    $user = new Users($data);
//    $user->save();
//

$users = Users::find();
foreach ($users as $user) {
    $ssn = $user->reference_id;
    $accounts = \backend\entities\Accounts::findByCustomerIdAndType($ssn);

    foreach ($accounts as &$account) {
        $dnb_transactions = Transactions::getDnbTransactions($account->accountNumber, $ssn);
        foreach ($dnb_transactions as $trans) {
            $merchant = Transactions::getMerchantByTransactionMessage($trans['message/KID']);
            $transaction = new Transactions([
                'user_id' => $user->id,
                'merchant_id' => $merchant->id,
                'amount' => ((float) $trans['amount']) * -1,
                'currency' => 'NOK',
                'date' => date('Ymd', strtotime($trans['timeStamp']))
            ]);
            $transaction->save();
        }

    }

    echo '.';
//    if ($limit % 10 === 0) echo PHP_EOL;
//    print_r($user->jsonSerialize());
}
//fclose($file);

