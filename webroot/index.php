<?php

use alkemann\h2l\exceptions\InvalidUrl;
use alkemann\h2l\Log;
use alkemann\h2l\Request;
use alkemann\h2l\Response;
use alkemann\jsonapi\response\Error;
use alkemann\h2l\util\Chain;
use backend\Api;

define('FIRST_APP_CONSTANT', 'FIRST_APP_CONSTANT');

$root_path = realpath(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR;
$vendor_path = $root_path . 'vendor' . DIRECTORY_SEPARATOR;
$config_path = $root_path . 'configs' . DIRECTORY_SEPARATOR;

require_once $vendor_path . 'autoload.php';

require_once $config_path . 'error_handlers.php';
set_exception_handler('api\handleError');
set_error_handler('api\handleWarning', E_WARNING);

require_once $config_path . 'environments.php';
require_once $config_path . 'routes.php';

\alkemann\h2l\Environment::put('data_folder', $root_path  . 'data');
Log::handler('standard', [Log::class, 'std']);

$dispatch = new alkemann\h2l\Dispatch($_REQUEST, $_SERVER, $_GET, $_POST);
$dispatch->setRouteFromRouter();

$dispatch->registerMiddle([Api::class, 'requestMiddleware']);
$dispatch->registerMiddle(function(Request $request, Chain $chain): Response {
    if (Api::isUrlAPIRequest($request->url()) === false) {
        return $chain->next($request);
    }
    try {
        $response = $chain->next($request);
    } catch (InvalidUrl $e) {
        return new Error([
            ['status' => 404, 'code' => $e->getCode(), 'detail' => $e->getMessage()]
        ], 404);
    } catch (\Throwable $e) {
        return new Error([
            ['status' => 500, 'code' => $e->getCode(), 'detail' => $e->getMessage()]
        ], 500);
    }
    return $response;
});

$response = $dispatch->response();
if ($response) {
    echo $response->render();
}
